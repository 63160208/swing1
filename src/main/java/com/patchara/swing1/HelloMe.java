package com.patchara.swing1;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user1
 */

class MyActionListener implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e){
        System.out.println("MyActionListener : Action");
    }
}

public class HelloMe {
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel IbYourName = new JLabel("Your name: ");
        IbYourName.setSize(200, 20);
        IbYourName.setLocation(5, 5);
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(100, 5);        

        JButton btnHello = new JButton("Hello");
        btnHello.setSize(100, 20);
        btnHello.setLocation(200,100);
        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        
        JLabel IblHello = new JLabel("Hello...",JLabel.CENTER);
        IblHello.setSize(200, 20);
        IblHello.setLocation(155, 150);
        IblHello.setBackground(Color.white);
        IblHello.setOpaque(true);
        
        frmMain.setLayout(null);
        
        frmMain.add(IbYourName);
        frmMain.add(txtYourName);
        frmMain.add(btnHello);
        frmMain.add(IblHello);
        
        btnHello.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourName.getText();
                IblHello.setText("Hello "+name);
            }
        });
        
        
        frmMain.setVisible(true);
        
    }
}
