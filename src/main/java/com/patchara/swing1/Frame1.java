/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author user1
 */
public class Frame1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        JLabel IbHelloWorld = new JLabel("Hello World!",JLabel.CENTER);
        IbHelloWorld.setBackground(Color.GREEN);
        IbHelloWorld.setOpaque(true);
        IbHelloWorld.setFont(new Font("Verdana",Font.PLAIN,30));
        frame.add(IbHelloWorld);
        frame.setVisible(true);
    }
    
}
